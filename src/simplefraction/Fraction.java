package simplefraction;

public class Fraction {
private int numerateur;
private int denominateur;
	public Fraction(int n,int d) {
		
		this.numerateur=n; 
		this.denominateur=d;
	
		}
	
	public int getNumerateur() {
		return numerateur;
	}
	public void setNumerateur(int numerateur) {
		this.numerateur = numerateur;
	}
	public int getDenominateur() {
		if (denominateur!=0) {
			return denominateur;
		}
		else {
			throw new RuntimeException();
		}
	}
	public void setDenominateur(int denominateur) {
		this.denominateur = denominateur;
	}
	@Override
	public String toString() {
		return "La fraction est: "+getNumerateur()+"/"+getDenominateur()+"="+ numerateur/denominateur;
	}
	
	
	

}
